
IMPORTANT
---------

PHP5 only - at least 5.1.6 - 5.2.x recommended

DESCRIPTION
-----------

This module adds a security layer to Drupal based on PHPIDS (www.phpids.org).
With a defined set or rules, it tries to detect malicious input from the (anonymous)
user - it does not strip, filter or sanitize the input. It logs directly to watchdog 
or syslog (if enabled), so you have a clear view on who's trying to break your site. 
It can send out a mail after a certain level of impact has been reached or display the 
user a blocking page thus making his action completely worthless. Although the 
functionality is there to redirect users after a certain impact.

INSTALLATION
------------

1) Download the latest PHPIDS package from http://www.phpids.org and extract the
   PHPIDS library into your Drupal libraries directory (example: sites/all/libraries).
2) Rename PHPIDS library directory to phpids (from default phpids-x.y).
3) Activate Drupal PHPIDS module in your module list.
4) Check status report that your Drupal PHPIDS module is running correctly.
4) Configure and test Drupal PHPIDS module. See CONFIGURATION AND TESTING.
   
There is a Config.ini in the PHPIDS library lib/IDS/Config folder, do not worry,
it's harmless, just keep it like it is.

UPDATE
----------

ATTENTION: Currently there is NO UPGRADE PATH from a lower version of the PHPIDS module!


CONFIGURATION AND TESTING
-------------------------

After that, enable the module and surf to the settings page on
http://yourdrupal/admin/settings/phpids and change the default
settings to your needs.

The Drupal status report informs you that Drupal PHPIDS module is correctly
running or not.

In addition, it is possible to set html-, json-included or exclude fields and variables
from scanning by PHPIDS. So you have the opportunity to finetune PHPIDS to your
website and you could reduce false positives.

Test if PHPIDS starts logging (not as user 1)

* normal log level
  http://yourdrupal/?phpidstest=">XXX
* warn & mail level - if you filled in an email
  http://yourdrupal/?phpidstest=">XXX<
* blocking level - redirects the (anonymous) user
  http://yourdrupal/?phpidstest=">XXX<"><script>

You should see the attacks logged in your dblog or in your syslog file.

BUGS, PATCHES AND FEATURE REQUESTS 
--------------

Please post bugs, patches and feature requests the issue queue on
http://drupal.org/project/phpids
