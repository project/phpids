<?php

/**
 * @file
 * PHPIDS module's administrative functions.
 */

/**
 * Form callback of administrative configuration form.
 */
function phpids_settings_form($form, &$form_state) {

  // Get all roles that haven't got the bypass permission.
  $untrusted_roles = user_roles();
  $trusted_roles = user_roles(FALSE, 'bypass phpids');
  foreach (array_intersect_assoc($untrusted_roles, $trusted_roles) as $key => $val) {
    unset($untrusted_roles[$key]);
  }

  // Vertical tabs.
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
  );

  // Reaction settings.
  $form['settings']['reaction'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reaction and thresholds'),
  );

  // Reaction: Send alert mails.
  $form['settings']['reaction']['phpids_reaction_send_mails'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send alert mails'),
    '#default_value' => variable_get('phpids_reaction_send_mails', TRUE),
  );
  $form['settings']['reaction']['mail_thresholds'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="phpids_reaction_send_mails"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['reaction']['mail_thresholds']['phpids_reaction_send_mails_impact'] = array(
    '#type' => 'textfield',
    '#title' => t('Impact threshold'),
    '#default_value' => variable_get('phpids_reaction_send_mails_impact', 9),
  );
  $form['settings']['reaction']['mail_thresholds']['phpids_reaction_send_mails_attempts'] = array(
    '#type' => 'textfield',
    '#title' => t('Attempts threshold'),
    '#default_value' => variable_get('phpids_reaction_send_mails_attempts', 1),
  );
  
  // Reaction: Warn the attacker.
  $form['settings']['reaction']['phpids_reaction_warning_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('Warn the attacker'),
    '#default_value' => variable_get('phpids_reaction_warning_message', TRUE),
  );
  $form['settings']['reaction']['warn_thresholds'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="phpids_reaction_warning_message"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['reaction']['warn_thresholds']['phpids_reaction_warning_message_impact'] = array(
    '#type' => 'textfield',
    '#title' => t('Impact threshold'),
    '#default_value' => variable_get('phpids_reaction_warning_message_impact', 10),
  );
  $form['settings']['reaction']['warn_thresholds']['phpids_reaction_warning_message_attempts'] = array(
    '#type' => 'textfield',
    '#title' => t('Attempts threshold'),
    '#default_value' => variable_get('phpids_reaction_warning_message_attempts', 2),
  );

  // Reaction: Block the attacker.
  $form['settings']['reaction']['phpids_reaction_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block the attacker'),
    '#default_value' => variable_get('phpids_reaction_block', TRUE),
  );
  $form['settings']['reaction']['block_thresholds'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="phpids_reaction_block"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['reaction']['block_thresholds']['phpids_reaction_block_impact'] = array(
    '#type' => 'textfield',
    '#title' => t('Impact threshold'),
    '#default_value' => variable_get('phpids_reaction_block_impact', 27),
  );
  $form['settings']['reaction']['block_thresholds']['phpids_reaction_block_attempts'] = array(
    '#type' => 'textfield',
    '#title' => t('Attempts threshold'),
    '#default_value' => variable_get('phpids_reaction_block_attempts', 10),
  );

  // Mail settings.
  $form['settings']['mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mailing'),
  );

  // Mail: Send alert to roles.
  $form['settings']['mail']['phpids_mail_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Send mail alerts to all users of a role.'),
    '#options' => user_roles(),
    '#default_value' => variable_get('phpids_mail_roles', array()),
  );

  // Mail: Addresses to notify.
  $form['settings']['mail']['phpids_mail_addresses'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail addresses to notify on intrusion detections'),
    '#description' => t('Put each address on a separate line. Site administrator is warned per default.'),
    '#rows' => 3,
    '#default_value' => implode(PHP_EOL, variable_get('phpids_mail_addresses', array(variable_get('site_mail')))),
  );

  // Mail: Subject and body.
  $form['settings']['mail']['mail_contents'] = array(
    '#type' => 'container',
  );
  $form['settings']['mail']['mail_contents']['phpids_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('phpids_mail_subject', t('PHPIDS intrusion alert')),
  );
  $form['settings']['mail']['mail_contents']['phpids_mail_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => variable_get('phpids_mail_body', t('An intrusion was detected on your site (@site_name). Please have a look at the recent log messages (!link) for further information. If you get this emails too often you should think about lowering the sensibility of PHPIDS.', array(
      '@site_name' => variable_get('site_name', $_SERVER['SERVER_NAME']),
      '!link' => url('admin/reports/dblog', array('absolute' => TRUE)),
    ))),
    '#rows' => 4,
  );

  // Warning settings.
  $form['settings']['warning'] = array(
    '#type' => 'fieldset',
    '#title' => t('Warning message'),
  );

  // Warning: Warn all users.
  $form['settings']['warning']['phpids_warn_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Warn all users.'),
    '#default_value' => variable_get('phpids_warn_all', TRUE),
  );

  // Warning: Warn users of role.
  $form['settings']['warning']['phpids_warn_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Warn users of role.'),
    '#options' => $untrusted_roles,
    '#default_value' => variable_get('phpids_warn_roles', array()),
    '#states' => array(
      'visible' => array(
        ':input[name="phpids_warn_all"]' => array('checked' => FALSE),
      ),
    ),
  );

  // Warning: Warn text.
  $form['settings']['warning']['phpids_warn_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Warning message text'),
    '#rows' => 3,
    '#default_value' => variable_get('phpids_warn_text', 'Note that an administrator was notified about your intrusion attempt.'),
  );

  // Blocking settings.
  $form['settings']['block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blocking'),
  );

  // Blocking: Block all users.
  $form['settings']['block']['phpids_block_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block all users.'),
    '#default_value' => variable_get('phpids_block_all', TRUE),
  );

  // Blocking: Block users of role.
  $form['settings']['block']['phpids_block_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Block users of role.'),
    '#options' => $untrusted_roles,
    '#default_value' => variable_get('phpids_block_roles', array()),
    '#states' => array(
      'visible' => array(
        ':input[name="phpids_block_all"]' => array('checked' => FALSE),
      ),
    ),
  );

  // Blocking: Block permanently.
  $form['settings']['block']['phpids_block_permanently'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block IP permanently by Drupal'),
    '#default_value' => variable_get('phpids_block_permanently', FALSE),
  );

  // Blocking: Block message title and text.
  $form['settings']['block']['block_message_page'] = array(
    '#type' => 'container',
  );
  $form['settings']['block']['block_message_page']['phpids_block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Block message title'),
    '#default_value' => variable_get('phpids_block_title', 'Security warning'),
  );
  $form['settings']['block']['block_message_page']['phpids_block_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Block message text'),
    '#rows' => 3,
    '#default_value' => variable_get('phpids_block_text', 'Your request to this website has been blocked. Please inform the webmaster of this site if you think that your access has been blocked by mistake.'),
  );

  // Filter settings.
  $form['settings']['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter'),
  );

  // Filter: Filters per field.
  $form['settings']['filter']['phpids_filter_html'] = array(
    '#type' => 'textarea',
    '#title' => t('HTML fields'),
    '#rows' => 3,
    '#default_value' => implode(PHP_EOL, variable_get('phpids_filter_html', array())),
  );
  $form['settings']['filter']['phpids_filter_json'] = array(
    '#type' => 'textarea',
    '#title' => t('JSON fields'),
    '#rows' => 3,
    '#default_value' => implode(PHP_EOL, variable_get('phpids_filter_json', array())),
  );
  $form['settings']['filter']['phpids_filter_excluded'] = array(
    '#type' => 'textarea',
    '#title' => t('Excluded fields'),
    '#rows' => 3,
    '#default_value' => implode(PHP_EOL, variable_get('phpids_filter_excluded', array())),
  );

  // Test mode settings.
  $form['settings']['test'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test mode'),
  );

  // Test mode.
  $form['settings']['test']['phpids_testmode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable test mode'),
    '#default_value' => variable_get('phpids_testmode', FALSE),
  );

  // Test mode: options.
  $form['settings']['test']['options'] = array(
    '#type' => 'container',
  );
  $form['settings']['test']['options']['phpids_testmode_print_findings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Print all detected findings to screen.'),
    '#default_value' => variable_get('phpids_testmode_print_findings', FALSE),
    '#states' => array(
      'enabled' => array(
        ':input[name="phpids_testmode"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['test']['options']['phpids_testmode_add_exception_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide an "add exception" link.'),
    '#default_value' => variable_get('phpids_testmode_add_exception_link', FALSE),
    '#states' => array(
      'enabled' => array(
        ':input[name="phpids_testmode"]' => array('checked' => TRUE),
        ':input[name="phpids_testmode_print_findings"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['test']['options']['phpids_testmode_log_rules'] = array(
    '#type' => 'checkbox',
    '#title' => t('Also add regular expression rules to reports.'),
    '#default_value' => variable_get('phpids_testmode_log_rules', FALSE),
    '#states' => array(
      'enabled' => array(
        ':input[name="phpids_testmode"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Advanced settings.
  $form['settings']['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
  );
  $form['settings']['advanced']['phpids_temp_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom temporary path'),
    '#default_value' => variable_get('phpids_temp_path', 'temporary://phpids'),
  );
  $form['settings']['advanced']['phpids_filter_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update default_filter.xml on cron run.'),
    '#default_value' => variable_get('phpids_filter_update', FALSE),
  );

  $form = system_settings_form($form);

  // Custom validation callback for the email notification setting.
  $form['#validate'][] = 'phpids_settings_form_validate';
  // We need to call our own submit callback first, not the one from
  // system_settings_form(), so that we can process and save the emails.
  unset($form['#submit']);

  return $form;
}

/**
 * Custom validation handler to check the notification emails.
 */
function phpids_settings_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['phpids_mail_addresses'])) {
    $valid = array();
    $invalid = array();
    // Get all entered emails.
    foreach (explode(PHP_EOL, trim($form_state['values']['phpids_mail_addresses'])) as $email) {
      $email = trim($email);
      // Check if all emails are well formatted.
      if (!empty($email)) {
        if (valid_email_address($email)) {
          $valid[] = $email;
        }
        else {
          $invalid[] = $email;
        }
      }
    }
    if (empty($invalid)) {
      $form_state['phpids_mail_addresses'] = $valid;
    }
    // Throw an error about invalid email addresses.
    elseif (count($invalid) == 1) {
      form_set_error('phpids_mail_addresses', t('%email is not a valid e-mail address.', array('%email' => reset($invalid))));
    }
    else {
      form_set_error('phpids_mail_addresses', t('%emails are not valid e-mail addresses.', array('%emails' => implode(', ', $invalid))));
    }
  }
}

/**
 * Custom submit handler to process and save the notification emails.
 */
function phpids_settings_form_submit($form, $form_state) {
  // Save email addresses. @see phpids_settings_form_validate().
  if (empty($form_state['phpids_mail_addresses'])) {
    variable_del('phpids_mail_addresses');
  }
  else {
    variable_set('phpids_mail_addresses', $form_state['phpids_mail_addresses']);
  }
  unset($form_state['phpids_mail_addresses']);
  unset($form_state['values']['phpids_mail_addresses']);

  // Save all the other textareas as arrays where necessary.
  variable_set('phpids_filter_html', explode(PHP_EOL, trim($form_state['values']['phpids_filter_html'])));
  unset($form_state['values']['phpids_filter_html']);
  variable_set('phpids_filter_json', explode(PHP_EOL, trim($form_state['values']['phpids_filter_json'])));
  unset($form_state['values']['phpids_filter_json']);
  variable_set('phpids_filter_excluded', explode(PHP_EOL, trim($form_state['values']['phpids_filter_excluded'])));
  unset($form_state['values']['phpids_filter_excluded']);

  // Save the rest like using system_settings_form() normally.
  system_settings_form_submit($form, $form_state);
}
